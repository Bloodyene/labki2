﻿using System;
using System.Collections.Generic;
using Lab1.Implementation;
using Lab1.Contract;

namespace Lab1.Main
{
    public class Program
    {
        public IList<IOwoc> Metoda()
        {
            Impl1 impl11 = new Impl1();
            Impl1 impl12 = new Impl1();
            Impl1 impl13 = new Impl1();
            Impl2 impl21 = new Impl2();
            Impl2 impl22 = new Impl2();

            List<IOwoc> lista = new List<IOwoc> { impl11, impl12, impl13, impl21, impl22 };
            return lista;
        }

        public void Metoda2(List<IOwoc> owoc)
        {
            for (int i = 0; i < owoc.Count; i++)
            {
                owoc[i].Metodka();
            }
            
        }

        static void Main(string[] args)
        {
            Class1 c = new Class1();
            c.InnaMetodka();
            Console.WriteLine(c.Metodka());
            Console.WriteLine((c as IOwoc).Metodka());
            Console.ReadKey();
        }
    }
}
