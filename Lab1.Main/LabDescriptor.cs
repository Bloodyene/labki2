﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IOwoc);
        
        public static Type ISub1 = typeof(IPomarańcza);
        public static Type Impl1 = typeof(Impl1);
        
        public static Type ISub2 = typeof(IJabłko);
        public static Type Impl2 = typeof(Impl2);
        
        
        public static string baseMethod = "Metodka";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "MetPom";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "MetJab";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Metoda";
        public static string collectionConsumerMethod = "Metoda2";

        #endregion

        #region P3

        public static Type IOther = typeof(IMaszyna);
        public static Type Impl3 = typeof(Class1);

        public static string otherCommonMethod = "Metodka";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
