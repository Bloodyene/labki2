﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

using Lab1.Main;
using Lab1.Contract;
using Lab1.Implementation;
using PK.Test;

namespace Lab1Test
{
    [TestFixture]
    [TestClass]
    public class P3_Implemantations
    {        
        #region IOther

        [Test]
        [TestMethod]
        public void P3__IOther_Should_Be_An_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.IOther.IsInterface);
        }

        [Test]
        [TestMethod]
        public void P3__IOther_Should_Have_At_Least_2_Methods()
        {
            Helpers.Should_Have_At_Least_Methods(LabDescriptor.IOther, 2);
        }

        [Test]
        [TestMethod]
        public void P3__IOther_Should_Have_OtherCommonMethod()
        {
            Helpers.Should_Have_Method(LabDescriptor.IOther, LabDescriptor.otherCommonMethod);
        }

        #endregion

        #region IBase

        [Test]
        [TestMethod]
        public void P3__IBase_Should_Have_OtherCommonMethod()
        {
            Helpers.Should_Have_Method(LabDescriptor.IBase, LabDescriptor.otherCommonMethod);
        }

        #endregion

        #region Impl3

        [Test]
        [TestMethod]
        public void P3__Impl3_Should_Be_A_Class()
        {
            // Assert
            Assert.That(LabDescriptor.Impl3.IsClass);
        }

        [Test]
        [TestMethod]
        public void P3__Impl3_Should_Inherit_From_ISub1()
        {
            Helpers.Should_Implement_Interface(LabDescriptor.Impl3, LabDescriptor.ISub1);
        }

        [Test]
        [TestMethod]
        public void P3__Impl3_Should_Inherit_From_IOther()
        {
            Helpers.Should_Implement_Interface(LabDescriptor.Impl3, LabDescriptor.IOther);
        }

        [Test]
        [TestMethod]
        public void P3__Impl3_Should_Implement_OtherCommonMethod()
        {
            Helpers.Should_Implement_Method(LabDescriptor.Impl3, LabDescriptor.otherCommonMethod, LabDescriptor.otherCommonMethodParams);
        }

        [Test]
        [TestMethod]
        public void P3__Impl3_Should_Implement_OtherCommonMethod_In_3_Different_Ways()
        {
            // Arrange
            var method1 = LabDescriptor.IBase.GetMethod(LabDescriptor.otherCommonMethod);
            var method2 = LabDescriptor.IOther.GetMethod(LabDescriptor.otherCommonMethod);
            var method3 = LabDescriptor.Impl3.GetMethod(LabDescriptor.otherCommonMethod);
            var instance = Activator.CreateInstance(LabDescriptor.Impl3);

            // Act
            var result1 = method1.Invoke(instance, LabDescriptor.otherCommonMethodParams);
            var result2 = method2.Invoke(instance, LabDescriptor.otherCommonMethodParams);
            var result3 = method3.Invoke(instance, LabDescriptor.otherCommonMethodParams);
            
            // Assert
            Assert.That(result1, Is.Not.EqualTo(result2));
            Assert.That(result2, Is.Not.EqualTo(result3));
            Assert.That(result3, Is.Not.EqualTo(result1));
        }

        [Test]
        [TestMethod]
        public void P3__Impl3_Should_Have_Stable_Implementation_Of_OtherCommonMethod()
        {
            // Arrange
            var method1 = LabDescriptor.IBase.GetMethod(LabDescriptor.otherCommonMethod);
            var method2 = LabDescriptor.IOther.GetMethod(LabDescriptor.otherCommonMethod);
            var method3 = LabDescriptor.Impl3.GetMethod(LabDescriptor.otherCommonMethod);
            var instance = Activator.CreateInstance(LabDescriptor.Impl3);

            // Act
            var result11 = method1.Invoke(instance, LabDescriptor.otherCommonMethodParams);
            var result12 = method2.Invoke(instance, LabDescriptor.otherCommonMethodParams);
            var result13 = method3.Invoke(instance, LabDescriptor.otherCommonMethodParams);
            var result23 = method3.Invoke(instance, LabDescriptor.otherCommonMethodParams);
            var result22 = method2.Invoke(instance, LabDescriptor.otherCommonMethodParams);
            var result21 = method1.Invoke(instance, LabDescriptor.otherCommonMethodParams);

            // Assert
            Assert.That(result11, Is.EqualTo(result21));
            Assert.That(result12, Is.EqualTo(result22));
            Assert.That(result13, Is.EqualTo(result23));
        }

        #endregion
    }
}


